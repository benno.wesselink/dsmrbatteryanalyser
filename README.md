# DSMR Battery Analyser

This Jupyter Notebook calculates the effect of a home battery on electrical energy delivered by and returned to the electrical grid. The initial purpose is to arrive at a cost reduction by kWh battery chart. This can then be used for investment analysis to calculate a pay back period.

## Prerequisites
This notebook uses the postgresql database of the [dsmr-reader](https://github.com/dsmrreader/dsmr-reader) project as data source. More specifically it uses the minutely aggregated measurements of total power delivered and returned to the electrical network. You can use any datasource provided you do small adaptations to the notebook.

## Installation
You'll need [Jupyter Notebooks](https://jupyter.org/). I would advise to install the notebooks by installing [Anaconda](https://www.anaconda.com/). Then you need to install several dependencies/packages. Below is a not limitative list (I'm  not sure what you need to install and what is installed by default):
- psycopg2
- dotenv 
- pandas 
- numpy

## Configuration
First you need to adapt the .env file so that you have the right connection parameters to your dsmr database. Then you need to adapt several parameters in the notebook to suit your analysis. See references below. You can also leave some at the default values if you don't know the real value.

```
# Change below depending on your situation
start_analysis = datetime.datetime(2020, 12, 1)
end_analysis = datetime.datetime.now()
```

```
# set battery efficiencies
charging_eff = 0.95
discharging_eff = 0.95
```

```
battery_sizes = [1,2,3,4,5,6,8,10,15,20,30]
```

```
# Change below depending on your situation
cost_delivered_energy = 0.27
gain_returned_energy = 0.045
```
